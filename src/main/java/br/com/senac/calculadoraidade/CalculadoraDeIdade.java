package br.com.senac.calculadoraidade;

public class CalculadoraDeIdade {

    public static void main(String... a) {

        Pessoa p1 = new Pessoa("DSchade", 18);
        Pessoa p2 = new Pessoa("Nunes", 21);
        Pessoa p3 = new Pessoa("Matheus", 44);

        String ResultadoPessoas;

        if ((p1.getIdade() > p2.getIdade()) && (p1.getIdade() > p3.getIdade())) {

            ResultadoPessoas = "Pessoa Mais Velha: ".concat(p1.getNome());
            System.out.println(ResultadoPessoas);

        } else if (p2.getIdade() > p3.getIdade()) {

            ResultadoPessoas = "Pessoa Mais Velha: ".concat(p2.getNome());
            System.out.println(ResultadoPessoas);

        } else {

            ResultadoPessoas = "Pessoa Mais Velha: ".concat(p3.getNome());
            System.out.println(ResultadoPessoas);

        }

    }
}


//System.out.println("Matheus".concat("\n Schade"));
